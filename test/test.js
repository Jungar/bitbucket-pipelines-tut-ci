const request = require('supertest')
const server = require('../server.js')
/* global describe, it */
describe('GET /', function () {
  it('displays "Hello World!"', function (done) {
    // The line below is the core test of our app.
    request(server).get('/').expect('Hello World!', () => {
      server.close()
      done.apply(arguments)
    })
  })
})
